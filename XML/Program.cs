﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace XML
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Students student;
                XmlSerializer ser = new XmlSerializer(typeof(Students));
                XmlReaderSettings settings = new XmlReaderSettings
                {
                    DtdProcessing = DtdProcessing.Parse,
                    ValidationType = ValidationType.DTD
                };
                using (XmlReader reader = XmlReader.Create("seccion.xml", settings))
                {
                    student = (Students)ser.Deserialize(reader);
                }

                var stInfo = new List<(StudentsStudent, StudentsStudentExamInfo)>();
                if (student != null)
                {
                    foreach (var s in student.Items)
                    {
                        foreach (var exam in s.ExamInfo)
                        {
                            stInfo.Add((s, exam));
                        }
                    }
                }
                foreach (var s in stInfo)
                {
                    Console.Write(
                     s.Item1.firstName + " " + s.Item1.lastName + " " +
                     s.Item1.patronymic + "\n" + s.Item1.BookNumber + "\n" +
                     s.Item2.subject + " " + s.Item2.date + " " + s.Item2.type + " " +
                     s.Item2.mark + "\n");
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
